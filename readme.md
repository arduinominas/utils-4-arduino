Utilidades para o Arduino
=== 
Seja qual for a versão ou modelo do Arduino sempre há tarefas que acabam ficando
repretivias, algumas funções do framework do arduino até ajudam, mas agente sempre
encontra uma forma diferente de fazer as coisas ou acaba tendo alguns códigos
que se fazem necessários.

Então estou colecionando aqui todas as funções que julgo interessantes serem
reaproveitadas em qualquer projeto.

Algumas foram testadas, outras apenas acordei um dia e escrevi achando que 
poderiam ser úteis em algum momento.

Inclusive aqui há funções para uso com sensores de todos os tipos.
Se você quer contribuir faça um fork do projeto e acrescente suas funções e me 
avise assim adiciono ao projeto original e compartilho com todos mantedo os 
créditos de autor.